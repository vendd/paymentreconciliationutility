﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Configuration;
using PaymentReconcilationUtilityDev;
using Renci.SshNet;

namespace PaymentReconcilationUtilityDev
{
    public class FTPClass
    {
        string activityLogsPath = System.Configuration.ConfigurationSettings.AppSettings["ActivityLogsPath"].ToString();

        string sFTPHost = ConfigurationSettings.AppSettings["SFTPHost"] ?? "";
        string sFTPUserName = ConfigurationSettings.AppSettings["SFTPUserName"] ?? "";
        string sFTPPWD = ConfigurationSettings.AppSettings["SFTPPWD"] ?? "";
        string sFTPPort = ConfigurationSettings.AppSettings["sFTPPort"] ?? "";

        string FTPHost = ConfigurationSettings.AppSettings["FTPHost"] ?? "";
        string FTPUserName = ConfigurationSettings.AppSettings["FTPUserName"] ?? "";
        string FTPPWD = ConfigurationSettings.AppSettings["FTPPWD"] ?? "";
        string FTPPort = ConfigurationSettings.AppSettings["FTPPort"] ?? "";

        bool IsSFTP = false;

        string url = null;

        public void FtpFileList()
        {
            try
            {
                IsSFTP = Convert.ToBoolean(ConfigurationSettings.AppSettings["IsSFTP"] ?? "false");
            }
            catch (Exception ex) { }

            try
            {
                List<string> FtpFileList = new List<string>();
                FtpWebRequest ftpRequest;
                if (IsSFTP)
                {
                    url = sFTPHost;
                    IEnumerable<Renci.SshNet.Sftp.SftpFile> objFileList;

                    using (SftpClient client = new SftpClient(url, Convert.ToInt16(sFTPPort), sFTPUserName, sFTPPWD))
                    {
                        client.Connect();

                        objFileList = client.ListDirectory("/");
                        foreach(Renci.SshNet.Sftp.SftpFile objFile in objFileList)
                        {
                            if (!objFile.FullName.ToUpper().EndsWith(".XLSX"))
                                continue;

                            FtpFileList.Add(objFile.FullName);
                        }
                    }
                }
                else
                {
                    url = "ftp://" + FTPHost + "/NBIC";
                    ftpRequest = (FtpWebRequest)FtpWebRequest.Create(url);
                    ftpRequest.Credentials = new NetworkCredential(FTPUserName, FTPPWD);
                    ftpRequest.UseBinary = true;
                    ftpRequest.UsePassive = true;
                    ftpRequest.KeepAlive = true;
                    ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;

                    FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                    StreamReader streamReader = new StreamReader(ftpRequest.GetResponse().GetResponseStream());
                    {
                        string fileName = streamReader.ReadLine();
                        while (fileName != null)
                        {
                            if (fileName.EndsWith("XLSX") || fileName.EndsWith(".xlsx"))
                            {
                                FtpFileList.Add(fileName);
                                fileName = streamReader.ReadLine();
                            }
                        }
                    }

                    streamReader.Close();

                    ftpResponse.Close();
                    ftpResponse.Dispose();
                }

                FTPFileDownload(FtpFileList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FTPFileDownload(List<string> FtpFileList)
        {
            string basePath = System.AppDomain.CurrentDomain.BaseDirectory;
            string countFilePath = ConfigurationSettings.AppSettings["CountFilePath"];
            string smtpMail = ConfigurationSettings.AppSettings["smtpMail"];
            string smtpPassword = ConfigurationSettings.AppSettings["smtpPassword"];
            string smtpPort = ConfigurationSettings.AppSettings["smtpPort"];
            string smtpServer = ConfigurationSettings.AppSettings["smtpServer"];
            string msgFolder = ConfigurationSettings.AppSettings["MsgDownloadPath"];

            bool IsInserted = false;

            try
            {
                IsSFTP = Convert.ToBoolean(ConfigurationSettings.AppSettings["IsSFTP"] ?? "false");
            }
            catch (Exception ex) { }

            try
            {
                string path = System.AppDomain.CurrentDomain.BaseDirectory;
                string destinationPathWithFileName = "";
                string msgname = "";

                foreach(string ftpfiles in FtpFileList)
                {
                    destinationPathWithFileName = basePath + "\\" + ftpfiles;
                    FileStream localFileStream;
                    #region FTP File Download Code

                    if (IsSFTP)
                    {
                        url = sFTPHost;

                        if (!ftpfiles.ToUpper().EndsWith(".XLSX"))
                            continue;

                        using (SftpClient client = new SftpClient(url, Convert.ToInt16(sFTPPort), sFTPUserName, sFTPPWD))
                        {
                            client.Connect();
                            MemoryStream ms = new MemoryStream();

                            client.DownloadFile("/" + ftpfiles, ms);
                            //write to file
                            FileStream file = new FileStream(basePath + "\\" + ftpfiles, FileMode.Create, FileAccess.Write);
                            ms.WriteTo(file);
                            file.Close();
                            ms.Close();
                        }
                    }
                    else
                    {
                        url = "ftp://" + FTPHost + "/NBIC/" + ftpfiles;
                        FtpWebRequest ftpRequest = (FtpWebRequest)FtpWebRequest.Create(url);
                        ftpRequest.Credentials = new NetworkCredential(FTPUserName, FTPPWD);
                        ftpRequest.UseBinary = true;
                        ftpRequest.UsePassive = true;
                        ftpRequest.KeepAlive = true;


                        ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                        //ftpRequest.EnableSsl = true;

                        //ServicePointManager.ServerCertificateValidationCallback = (sender, cert, chain, errors) => true;
                        Console.WriteLine("Connecting To FTP Server...." + FTPHost);
                        FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();

                        localFileStream = new FileStream(destinationPathWithFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                        byte[] b = null;
                        Stream stream = ftpResponse.GetResponseStream();
                        MemoryStream ms = new MemoryStream();

                        Console.WriteLine("Downloading the file....");
                        int count = 0;
                        do
                        {
                            byte[] buf = new byte[1024];
                            count = stream.Read(buf, 0, 1024);
                            ms.Write(buf, 0, count);
                            // localFileStream.Write();

                        } while (stream.CanRead && count > 0);
                        b = ms.ToArray();
                        ms.Position = 0;
                        ms.CopyTo(localFileStream);

                        string status = ftpResponse.StatusDescription;

                        CommonLog.WriteLog("FTP File" + status);
                        CommonLog.WriteLog("FTP File Name :" + ftpfiles);

                        localFileStream.Close();
                        localFileStream.Dispose();

                        stream.Close();
                        stream.Dispose();

                        ftpResponse.Close();
                        ftpResponse.Dispose();
                    }

                    #endregion

                    #region insert claims

                    string FTPFilePath = destinationPathWithFileName;

                    if (!string.IsNullOrEmpty(FTPFilePath))
                    {
                        if ((new FileInfo(FTPFilePath)).Extension.ToUpper() == ".XLSX")
                        {
                            IsInserted = ReconcilationPayment.InsertPayment(FTPFilePath);
                        }
                    }
                    #endregion

                    #region delete file from FTP
                    CommonLog.WriteLog("FTP File Deleting...");

                    if (IsInserted == true)
                    {
                        FTPFileDelete(ftpfiles);
                    }

                    #endregion
                }                
            }
            catch (Exception ex)
            {
                CommonLog.WriteExceptionLog("Application Failed while downloading and creating file:" + ex.Message.ToString(), ex);
                //return null;
            }
        }

        private void FTPFileDelete(string ftpfiles)
        {
            try
            {
                IsSFTP = Convert.ToBoolean(ConfigurationSettings.AppSettings["IsSFTP"] ?? "false");
            }
            catch (Exception ex) { }

            try
            {
                if (IsSFTP)
                {
                    url = sFTPHost;
                    IEnumerable<Renci.SshNet.Sftp.SftpFile> objFileList;

                    using (SftpClient client = new SftpClient(url, Convert.ToInt16(sFTPPort), sFTPUserName, sFTPPWD))
                    {
                        client.Connect();
                        client.DeleteFile("/" + ftpfiles);
                    }
                }
                else
                {
                    url = "ftp://" + FTPHost + "/NBIC/" + ftpfiles;
                    FtpWebRequest ftpRequest = (FtpWebRequest)FtpWebRequest.Create(url);
                    ftpRequest.Credentials = new NetworkCredential(FTPUserName, FTPPWD);
                    ftpRequest.UseBinary = true;
                    ftpRequest.UsePassive = true;
                    ftpRequest.KeepAlive = true;
                    ftpRequest.Method = WebRequestMethods.Ftp.DeleteFile;

                    FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();

                    string status = ftpResponse.StatusDescription;

                    CommonLog.WriteLog("FTP File " + status);

                    ftpResponse.Close();
                    ftpResponse.Dispose(); 
                }
            }
            catch (Exception Ex)
            {
                CommonLog.WriteExceptionLog("Application Failed when deleting file:" + Ex.Message.ToString(), Ex);
            }

        }
    }
}
