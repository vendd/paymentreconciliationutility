﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using ExcelDataReader;



namespace PaymentImportUtility
{
    public class ExceptionLogDAL
    {
        public void ImportPayments()
        {
            string filePath = GetFile();
            InsertPayment(filePath);
        }

        public string LogFilePath { get; set; }

        public string GetFile()
        {
            return "D:\\ProjectsDocs\\Heritage Dev\\Auto Pay Reconciliation History-JK Draft1 (3).xlsx";
        }

        public bool InsertPayment(string FilePath)
        {
            WriteLog("---------------------------------------");
            WriteLog("Begin to Read excel file.");
            bool Result = false;
            DataSet ds = ReadXlsx(FilePath);
            WriteLog("Excel file read.");

            if (ds != null)
            {
                try
                {
                    string CANInvoiveNo, MarkedAsError, GrossPaymentDt = "";

                    int RowCount = ds.Tables[0].Rows.Count;

                    //---Open Db connection for once-----------------------
                    Payments objPayment = new Payments();
                    objPayment.OpenDbConnection();

                    WriteLog("Data inserting started.");

                    int i = 0;
                    //--------Started manuplating data one by one--------------
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        CANInvoiveNo = "";
                        MarkedAsError = "";

                        //----- To skip 1st of row of column mentioned in Excel sheet.
                        if (i == 0)
                        {
                            i = 1;
                            continue;
                        }

                        //-----To manuplate data of row which have "APPROVED" in column "Marked as Error".
                        MarkedAsError = Convert.ToString(row[8]).ToUpper();
                        if (MarkedAsError == "APPROVED")
                        {
                            objPayment.ClearFields();
                            try
                            {
                                GrossPaymentDt = (Convert.ToDateTime(row[12])).ToString();
                                CANInvoiveNo = Convert.ToString(row[3]);
                                objPayment.InvoiceId = CANInvoiveNo.Substring(("INV-").Length);
                                objPayment.AmountReceived = Convert.ToString(row[11]);
                                objPayment.ReceivedDate = Convert.ToString(row[12]);
                                objPayment.Save();
                            }
                            catch (Exception ex)
                            {
                                WriteExceptionLog("Error at inserting InvoiceNo: " + Convert.ToString(row[3]), ex);
                            }
                        }
                    }
                    objPayment.CloseDbConnection();
                    WriteLog("Data inserting ended.");
                }
                catch (Exception ex)
                {
                    WriteExceptionLog("Error at Opening DB Connection", ex);
                }
            }

            WriteLog("---------------------------------------");
            return Result;
        }

        public DataSet ReadXlsx(string FilePath)
        {
            DataSet result;
            using (var stream = File.Open(FilePath, FileMode.Open, FileAccess.Read))
            {
                // Auto-detect format, supports:
                //  - Binary Excel files (2.0-2003 format; *.xls)
                //  - OpenXml Excel files (2007 format; *.xlsx)
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    //  Use the AsDataSet extension method
                    result = reader.AsDataSet();
                    // The result of each spreadsheet is in result.Tables
                }
            }
            return result;
        }

        public void CreateLogFile(string lslogFileName)
        {
            string activityLogsPath = System.Configuration.ConfigurationManager.AppSettings["ActivityLogsPath"].ToString();
            System.IO.FileStream loFileStream = null;

            if (!string.IsNullOrEmpty(lslogFileName))
            {
                if (System.IO.Directory.Exists(activityLogsPath) == false)
                {
                    System.IO.Directory.CreateDirectory(activityLogsPath);
                }
                lslogFileName = activityLogsPath + "" + lslogFileName;
                if (!(System.IO.File.Exists(lslogFileName) == true))
                {
                    loFileStream = System.IO.File.Create(lslogFileName);
                }
                if ((loFileStream != null))
                {
                    loFileStream.Close();
                    loFileStream.Dispose();
                }
            }
        }

        public void WriteExceptionLog(string userMessage, Exception ex)
        {
            string logMessage = "User Message: " + userMessage + System.Environment.NewLine;
            logMessage += "System Exception Message: " + ex.Message + System.Environment.NewLine;
            if (ex.InnerException != null)
            {
                logMessage += "Inner Exception Message: " + ex.InnerException.Message + System.Environment.NewLine;
            }
            WriteLog(logMessage);
        }

        public void WriteLog(string messageToWrite)
        {
            System.IO.TextWriter loTextWriter = null;
            try
            {
                if (System.IO.File.Exists(this.LogFilePath) == true)
                {
                    loTextWriter = System.IO.File.AppendText(this.LogFilePath);
                    loTextWriter.WriteLine(DateTime.Now.ToString() + ": " + messageToWrite);
                    loTextWriter.Flush();
                    loTextWriter.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }
    }

    public class Payments
    {
        public string PaymentId { get; set; }
        public string InvoiceId { get; set; }
        public string ReceivedDate { get; set; }
        public string AmountReceived { get; set; }
        public string CheckNumber { get; set; }
        public string AddedBy { get; set; }

        SqlConnection SqlConn;

        public Payments()
        {
            ClearFields();
        }

        public void ClearFields()
        {
            this.PaymentId = "0";
            this.InvoiceId = "0";
            this.ReceivedDate = "";
            this.AmountReceived = "";
            this.CheckNumber = "";
            this.AddedBy = "0";
        }

        public void OpenDbConnection()
        {
            this.SqlConn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString:SQL"]);
            this.SqlConn.Open();
        }

        public void CloseDbConnection()
        {
            this.SqlConn.Close();
        }

        public void Save()
        {
            try
            {
                SqlCommand SqlCmd = new SqlCommand("InsertUpdatePayments", this.SqlConn);
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlCmd.Parameters.AddWithValue("@PaymentId", Convert.ToInt16(this.PaymentId));
                SqlCmd.Parameters["@PaymentId"].Direction = ParameterDirection.Output;

                SqlCmd.Parameters.AddWithValue("@InvoiceId", Convert.ToInt16(this.InvoiceId));
                if(!string.IsNullOrEmpty(this.ReceivedDate))
                    SqlCmd.Parameters.AddWithValue("@ReceivedDate", this.ReceivedDate);
                else
                    SqlCmd.Parameters.AddWithValue("@ReceivedDate", DBNull.Value);
                if (!string.IsNullOrEmpty(this.AmountReceived))
                    SqlCmd.Parameters.AddWithValue("@AmountReceived", this.AmountReceived);
                else
                    SqlCmd.Parameters.AddWithValue("@AmountReceived", DBNull.Value);
                SqlCmd.Parameters.AddWithValue("@CheckNumber", this.CheckNumber);
                SqlCmd.Parameters.AddWithValue("@AddedBy", Convert.ToInt16(this.AddedBy));
                SqlCmd.ExecuteNonQuery();
                PaymentId = Convert.ToString(SqlCmd.Parameters["@PaymentId"].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
