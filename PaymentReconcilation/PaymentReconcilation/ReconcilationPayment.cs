﻿using System;
using System.Data;
using System.Data.SqlClient;
using ExcelDataReader;
using System.IO;

namespace PaymentReconcilationUtilityDev
{
    class ReconcilationPayment
    {
        public static bool InsertPayment(string FilePath)
        {
            try
            {

                //string FilePath = Program.GetFile();
                CommonLog.WriteLog("-------------: Application Started :-------------------");
                CommonLog.WriteLog("Begin to Read excel file.");
                DataSet ds = ReadXlsx(FilePath);
                CommonLog.WriteLog("Excel file read.");

                if (ds != null)
                {
                    try
                    {
                        int RowCount = ds.Tables[0].Rows.Count;

                        //---Open Db connection for once-----------------------
                        Payments objPayment = new Payments();
                        objPayment.OpenDbConnection();

                        CommonLog.WriteLog("Data inserting started.");

                        int i = 0;
                        //--------Started manuplating data one by one--------------
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            //----- To skip 1st of row of column mentioned in Excel sheet.
                            if (i == 0)
                            {
                                i = 1;
                                continue;
                            }

                            //-----To manuplate data of row which have "APPROVED" in column "Marked as Error".
                            //MarkedAsError = Convert.ToString(row[8]).ToUpper();
                            //if (MarkedAsError == "APPROVED")
                            //{
                            //}
                            objPayment.ClearFields();
                            try
                            {
                                objPayment.PRID = Convert.ToInt32(row[11]);
                                objPayment.PaymentDate = Convert.ToDateTime(row[3]);
                                objPayment.WPPayment = Convert.ToDecimal(row[4]);
                                objPayment.Notes = Convert.ToString(row[5]);
                                objPayment.PayorName = Convert.ToString(row[9]);
                                objPayment.Flag = Convert.ToString(row[10]);
                                objPayment.Save();
                            }
                            catch (Exception ex)
                            {
                                CommonLog.WriteLog("Error at inserting InvoiceNo: " + Convert.ToString(row[3]) + ": " + ex.Message);
                            }
                        }
                        objPayment.CloseDbConnection();
                        CommonLog.WriteLog("Data inserting ended.");
                    }
                    catch (Exception ex)
                    {
                        CommonLog.WriteLog("Error at Opening DB Connection:" + ex.Message);
                        return false;
                    }
                }

                CommonLog.WriteLog("---------------------------------------");
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static DataSet ReadXlsx(string FilePath)
        {
            DataSet result;
            using (var stream = File.Open(FilePath, FileMode.Open, FileAccess.Read))
            {
                // Auto-detect format, supports:
                //  - Binary Excel files (2.0-2003 format; *.xls)
                //  - OpenXml Excel files (2007 format; *.xlsx)
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    //  Use the AsDataSet extension method
                    result = reader.AsDataSet();
                    // The result of each spreadsheet is in result.Tables
                }
            }
            return result;
        }

        public class Payments
        {
            public int PRID { get; set; }
            public int PAID { get; set; }
            public int HBID { get; set; }
            public string ClaimNumber { get; set; }
            public DateTime? ExtractionDate { get; set; }
            public decimal CanPaymentReq { get; set; }
            public string Vendor { get; set; }
            public string VendorUID { get; set; }
            public string Classification { get; set; }
            public string PayorName { get; set; }
            public DateTime? PaymentDate { get; set; }
            public decimal WPPayment { get; set; }
            public string Notes { get; set; }
            public string Flag { get; set; }

            SqlConnection SqlConn;

            public Payments()
            {
                ClearFields();
            }

            public void ClearFields()
            {
                this.PRID = 0;
                this.PAID = 0;
                this.HBID = 0;
                this.ClaimNumber = "";
                ExtractionDate = null;
                CanPaymentReq = 0;
                Vendor = "";
                VendorUID = "";
                Classification = "";
                PayorName = "";
                PaymentDate = null;
                WPPayment = 0;
                Notes = "";
                Flag = "";
            }

            public void OpenDbConnection()
            {
                this.SqlConn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString:SQL"]);
                this.SqlConn.Open();
            }

            public void CloseDbConnection()
            {
                this.SqlConn.Close();
            }

            public void Save()
            {
                try
                {
                    SqlCommand SqlCmd = new SqlCommand("InsertPaymentReconcilation", this.SqlConn);
                    SqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlCmd.Parameters.AddWithValue("@PRID", this.PRID);
                    SqlCmd.Parameters.AddWithValue("@PaymentDate", this.PaymentDate);
                    SqlCmd.Parameters.AddWithValue("@WPPayment", this.WPPayment);
                    SqlCmd.Parameters.AddWithValue("@Notes", this.Notes);
                    SqlCmd.Parameters.AddWithValue("@PayorName", this.PayorName);
                    SqlCmd.Parameters.AddWithValue("@Flag", this.Flag);
                    SqlCmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
