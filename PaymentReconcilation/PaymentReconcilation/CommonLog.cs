﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentReconcilationUtilityDev
{
    public static class CommonLog
    {
        static string activityLogsPath = System.Configuration.ConfigurationSettings.AppSettings["ActivityLogsPath"];
        static string lsLogFileName = "ActivityLog_" + DateTime.Now.AddDays(-1 * Convert.ToInt32(DateTime.Now.DayOfWeek)).ToString("MMddyyyy") + ".txt";

        static CommonLog()
        {
            System.IO.FileStream loFileStream = null;

            if (!string.IsNullOrEmpty(lsLogFileName))
            {
                if (System.IO.Directory.Exists(activityLogsPath) == false)
                {
                    System.IO.Directory.CreateDirectory(activityLogsPath);
                }
                lsLogFileName = activityLogsPath + "" + lsLogFileName;
                if (!(System.IO.File.Exists(lsLogFileName) == true))
                {
                    loFileStream = System.IO.File.Create(lsLogFileName);
                }
                if ((loFileStream != null))
                {
                    loFileStream.Close();
                    loFileStream.Dispose();
                }
            }
        }

        public static void WriteExceptionLog(string userMessage, Exception ex)
        {
            string logMessage = "User Message: " + userMessage + System.Environment.NewLine;
            logMessage += "System Exception Message: " + ex.Message + System.Environment.NewLine;
            if (ex.InnerException != null)
            {
                logMessage += "Inner Exception Message: " + ex.InnerException.Message + System.Environment.NewLine;
            }
            WriteLog(logMessage);
        }

        public static void WriteLog(string messageToWrite)
        {
           
            System.IO.TextWriter loTextWriter = null;
            try
            {
                if (System.IO.File.Exists(lsLogFileName))
                {
                    loTextWriter = System.IO.File.AppendText(lsLogFileName);

                    loTextWriter.WriteLine();
                    loTextWriter.WriteLine("--------------------------------------------------------------");
                    loTextWriter.WriteLine(DateTime.Now.ToString());
                    loTextWriter.WriteLine();
                    loTextWriter.WriteLine(messageToWrite);
                    loTextWriter.WriteLine("--------------------------------------------------------------");
                    loTextWriter.Flush();
                    loTextWriter.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
